//
//  ENHmacUtilities.h
//  WeatherBug
//
//  Created by David Holzer on 1/31/13.
//  Copyright (c) 2013 Earth Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ENHmacUtilities : NSObject

+ (NSString *)encodedGetUrlWithAuthId:(NSString *)authId
                            secretKey:(NSString *)secretKey
                              baseUrl:(NSString *)baseUrl
                                 path:(NSString *)path
                           parameters:(NSDictionary *)parameters;

+ (NSString *)encodedUrlWithAuthId:(NSString *)authId
                              verb:(NSString *)verb
                         secretKey:(NSString *)secretKey
                           baseUrl:(NSString *)baseUrl
                              path:(NSString *)path
                          postData:(NSString *)postData
                        parameters:(NSDictionary *)parameters;

+ (NSString *)hmacForMessage:(NSString *)message secretKey:(NSString *)secretKey;

+ (NSString *)hmacForMessage:(NSString *)message secretKey:(NSString *)secretKey base64Encoded:(BOOL)base64Encoded;

@end
