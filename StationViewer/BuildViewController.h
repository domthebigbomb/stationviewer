//
//  BuildViewController.h
//  StationViewer
//
//  Created by Dominic Ong on 7/10/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuildViewController : UIViewController

-(IBAction)backButtonPressed:(UIBarButtonItem *)button;
@property (weak, nonatomic) IBOutlet UILabel *buildLabel;
@end
