//
//  StationToolAppDelegate.h
//  StationViewer
//
//  Created by Dominic Ong on 6/20/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationToolAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
