//
//  StationPointAnnotation.h
//  StationViewer
//
//  Created by Dominic Ong on 7/9/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface StationPointAnnotation : NSObject<MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title, *subtitle;
- (id)initWithLocation:(CLLocationCoordinate2D)coord;
@property (nonatomic) NSString *stationID;
@property (nonatomic) NSString *stationLabel;

@end
