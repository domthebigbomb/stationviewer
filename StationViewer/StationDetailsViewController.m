//
//  StationDetailsViewController.m
//  StationViewer
//
//  Created by Dominic Ong on 7/3/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import "StationDetailsViewController.h"

@interface StationDetailsViewController ()

-(IBAction)dismissButtonPressed:(UIBarButtonItem *)button;

@end

@implementation StationDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_latitudeLabel setText:_latitude];
    [_longitudeLabel setText:_longitude];
    [_stationIdLabel setText:_stationId];
    [_stationNameLabel setText:_stationName];
    [_providerIdLabel setText:_providerId];
    [_providerNameLabel setText:_providerName];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismissButtonPressed:(UIBarButtonItem *)button{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
