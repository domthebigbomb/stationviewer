//
//  StationPointAnnotation.m
//  StationViewer
//
//  Created by Dominic Ong on 7/9/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import "StationPointAnnotation.h"

@implementation StationPointAnnotation

-(id)initWithLocation:(CLLocationCoordinate2D)coord{
    self = [super init];
    if (self){
        self ->_coordinate = coord;
    }
    
    return self;
}

@end
