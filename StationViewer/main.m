//
//  main.m
//  StationViewer
//
//  Created by Dominic Ong on 6/20/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StationToolAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([StationToolAppDelegate class]));
    }
}
