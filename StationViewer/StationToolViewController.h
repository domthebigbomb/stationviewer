//
//  StationToolViewController.h
//  StationViewer
//
//  Created by Dominic Ong on 6/20/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ENHmacUtilities.h"
#import <MapKit/MapKit.h>
#import "StationPointAnnotation.h"

@interface StationToolViewController : UIViewController <MKMapViewDelegate, UITextFieldDelegate>
-(IBAction)refreshStations:(UIButton *)button;
-(IBAction)showBuildDetails:(UIButton *)button;
@property (strong ,nonatomic) IBOutlet MKMapView *mapView;
@property (strong ,nonatomic) IBOutlet UILabel *coordinateLabel;
@property (weak, nonatomic) IBOutlet UITextField *latTextField;
@property (weak, nonatomic) IBOutlet UITextField *lonTextField;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UIButton *buildButton;
@end
