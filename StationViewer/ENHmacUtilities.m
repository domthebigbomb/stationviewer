//
//  ENHmacUtilities.m
//  WeatherBug
//
//  Created by David Holzer on 1/31/13.
//  Copyright (c) 2013 Earth Networks. All rights reserved.
//

#import <CommonCrypto/CommonHMAC.h>
#import "ENHmacUtilities.h"
#import "NSData+Base64.h"


NSString *const kHMAC_TS = @"timestamp";
NSString *const kHMAC_HASH = @"hash";
NSString *const kHMAC_ID = @"authid";


static NSString * Util_ENBase64EncodedStringFromData(NSData *data)
{
    NSUInteger length = [data length];
    NSMutableData *mutableData = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    
    uint8_t *input = (uint8_t *)[data bytes];
    uint8_t *output = (uint8_t *)[mutableData mutableBytes];
    
    for (NSUInteger i = 0; i < length; i += 3) {
        NSUInteger value = 0;
        for (NSUInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        static uint8_t const kAFBase64EncodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        
        NSUInteger idx = (i / 3) * 4;
        output[idx + 0] = kAFBase64EncodingTable[(value >> 18) & 0x3F];
        output[idx + 1] = kAFBase64EncodingTable[(value >> 12) & 0x3F];
        output[idx + 2] = (i + 1) < length ? kAFBase64EncodingTable[(value >> 6)  & 0x3F] : '=';
        output[idx + 3] = (i + 2) < length ? kAFBase64EncodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:mutableData encoding:NSASCIIStringEncoding];
}


/* NSString category for HMAC and URL string encoding */
@interface NSString (AWSAdditions)
- (NSString*)URLEncodedStringForCharacters:(NSString*)characters;
+ (NSData*)HMACSHA256EncodedDataWithKey:(NSString*)key data:(NSString*)data;
@end


@implementation NSString (AWSAdditions)
- (NSString*)URLEncodedStringForCharacters:(NSString*)characters
{
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)self, NULL, (__bridge CFStringRef)characters, kCFStringEncodingUTF8);
}

+ (NSData*)HMACSHA256EncodedDataWithKey:(NSString*)key data:(NSString*)data
{
    const char *cKey = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [data cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:CC_SHA256_DIGEST_LENGTH];
    return hash;
}
@end


@implementation ENHmacUtilities


+ (NSString *)hmacForMessage:(NSString *)message secretKey:(NSString *)secretKey
{
    return [[self class] hmacForMessage:message secretKey:secretKey base64Encoded:NO];
}

+ (NSString *)hmacForMessage:(NSString *)message secretKey:(NSString *)secretKey base64Encoded:(BOOL)base64Encoded
{
    NSData *hash = [NSString HMACSHA256EncodedDataWithKey:secretKey data:message];
    
    if (base64Encoded) {
//        return [hash base64EncodedString];
        // Use the other base64 encoding...
        // TODO: move it to a utility class
        return Util_ENBase64EncodedStringFromData(hash);
    }
    
    NSString *encoded = [hash description];
    encoded = [encoded stringByReplacingOccurrencesOfString:@"<" withString:@""];
    encoded = [encoded stringByReplacingOccurrencesOfString:@">" withString:@""];
    encoded = [encoded stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return encoded;
}

+ (NSString *)urlEncodedString:(NSString *)string
{
    return [string URLEncodedStringForCharacters:@" =!*'();:@+$,/?%#[]"];
}

+ (NSString *)encodedGetUrlWithAuthId:(NSString *)authId
                            secretKey:(NSString *)secretKey
                              baseUrl:(NSString *)baseUrl
                                 path:(NSString *)path
                           parameters:(NSDictionary *)parameters
{
    return [[self class] encodedUrlWithAuthId:authId
                                         verb:@"GET"
                                    secretKey:secretKey
                                      baseUrl:baseUrl
                                         path:path
                                     postData:@""
                                   parameters:parameters];
}

+ (NSString *)encodedUrlWithAuthId:(NSString *)authId
                              verb:(NSString *)verb
                         secretKey:(NSString *)secretKey
                           baseUrl:(NSString *)baseUrl
                              path:(NSString *)path
                          postData:(NSString *)postData
                        parameters:(NSDictionary *)parameters
{
    // Get time stamp
    NSString *timestamp = [NSString stringWithFormat:@"%0.0f", floorf([[NSDate date] timeIntervalSince1970])];

    // Sort the parameter keys
    NSArray *sortedKeys = [[parameters allKeys] sortedArrayUsingComparator:^(id obj1, id obj2) {
        return [obj1 localizedCaseInsensitiveCompare:obj2];
    }];

    // Assemble the hash data for verb, post data, URL
    NSMutableString *hashData = [NSMutableString stringWithFormat:@"%@\n%@\n%@\n%@", verb, path, postData, timestamp];
    
    // Append parameter key-value pairs with delimiters
    for (NSString *key in sortedKeys) {
        NSString *value = [parameters objectForKey:key];
        [hashData appendFormat:@"\n%@\n%@", key, value];
    }

    // Encode the message with secret key
    NSString *encoded = [[self class] hmacForMessage:hashData secretKey:secretKey base64Encoded:YES];
    
    // URL encode the HMAC encoded message
    encoded = [[self class] urlEncodedString:encoded];//[encoded URLEncodedStringForCharacters:@" =!*'();:@+$,/?%#[]"];
    
    NSMutableString *queryUrl = [NSMutableString stringWithFormat:@"%@%@?timestamp=%@", baseUrl, path, timestamp];
    for (NSString *key in sortedKeys) {
        NSString *value = [parameters objectForKey:key];
        // URL encode any parameters that need it
        value = [[self class] urlEncodedString:value];
        [queryUrl appendFormat:@"&%@=%@", key, value];
    }
    
    // Append the hash and authid to the url parameters
    [queryUrl appendFormat:@"&hash=%@&authid=%@", encoded, authId];
        
    return queryUrl;
}

@end
