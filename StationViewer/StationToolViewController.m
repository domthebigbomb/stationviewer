//
//  StationToolViewController.m
//  StationViewer
//
//  Created by Dominic Ong on 6/20/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import "StationToolViewController.h"
#import "StationDetailsViewController.h"

@interface StationToolViewController ()
@property (strong,nonatomic) UITapGestureRecognizer *tap;

@property (strong,nonatomic) NSArray *stationList;
@end

@implementation StationToolViewController{
    NSString *stationURLString;
    NSURL *stationURL;
    NSString *currentStation;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_mapView  setDelegate:self];
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = YES;
    [self.view addGestureRecognizer:_tap];
    _refreshButton.layer.cornerRadius = 5.0f;
    _refreshButton.layer.shadowRadius = 5.0f;
    _refreshButton.layer.shadowOpacity = 0.5f;
    _refreshButton.layer.shadowOffset = CGSizeMake(0, 5);
    //NSString *authId = @""
}

-(IBAction)showBuildDetails:(UIButton *)button{
    [self performSegueWithIdentifier:@"ShowBuild" sender:self];
}

-(IBAction)refreshStations:(UIButton *)button{
    [_mapView removeAnnotations:[_mapView annotations]];
    CLLocationCoordinate2D center;
    double latitude;
    double longitude;

    if([_latTextField.text length] == 0 || [_lonTextField.text length] == 0){
        MKCoordinateRegion currentRegion = [_mapView region];
        center = currentRegion.center;
        latitude = (double)center.latitude;
        longitude = (double)center.longitude;
    }else{
        latitude = [_latTextField.text doubleValue];
        longitude = [_lonTextField.text doubleValue];
        center = CLLocationCoordinate2DMake(latitude, longitude);
    }
    [_coordinateLabel setText:[NSString stringWithFormat:@"La,Lo: %.6f,%.6f",latitude,longitude]];
    stationURLString = @"http://qa.pulse.earthnetworks.com/data/locations/v3/stationlist?";
    stationURLString = [NSString stringWithFormat:@"%@latitude=%f&longitude=%f&verbose=true&units=english",stationURLString,latitude,longitude];
    //stationURLString = [ENHmacUtilities hmacForMessage:stationURLString secretKey: secretKey base64Encoded:YES];
    NSLog(@"Refreshing with URL: %@", stationURLString);
    stationURL = [NSURL URLWithString:stationURLString];
    NSURLRequest *stationRequest = [NSURLRequest requestWithURL:stationURL];
    [NSURLConnection sendAsynchronousRequest:stationRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
        NSLog(@"%@",[responseDict description]);
        NSArray *stations = [[responseDict objectForKey:@"result"] objectForKey:@"Stations"];
        _stationList = [[NSArray alloc] initWithArray:stations];
        for(NSDictionary *station in stations){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                CLLocationDegrees Lon = [[station objectForKey:@"Longitude"] doubleValue];
                CLLocationDegrees Lat = [[station objectForKey:@"Latitude"] doubleValue];
                CLLocationCoordinate2D stationLocation = CLLocationCoordinate2DMake(Lat,Lon);
                StationPointAnnotation *test = [[StationPointAnnotation alloc] init];
                [test setCoordinate:stationLocation];
                [test setTitle:[station objectForKey:@"StationName"]];
                [test setSubtitle:[station objectForKey:@"StationId"]];
                NSLog(@"Station Description: %@",[test description]);
                [_mapView addAnnotation: test];
            });
        }
    }];
}


-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"Station"];
    annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Station"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.canShowCallout = YES;
    annotationView.annotation = annotation;
    annotationView.animatesDrop = YES;
    return annotationView;
     
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    // Go to edit view
    NSArray *selectedAnnotation = [[NSArray alloc] initWithArray:[_mapView selectedAnnotations]];
    StationPointAnnotation *stationAnnotation = [selectedAnnotation firstObject];
    currentStation = [[NSString alloc]initWithString:stationAnnotation.subtitle];
    [self performSegueWithIdentifier:@"ShowDetails" sender:self];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _tap.enabled = YES;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:_latTextField]){
        [_lonTextField becomeFirstResponder];
    }else{
        [self performSelector:@selector(refreshStations:) withObject:nil];
        [self hideKeyboard];
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
}

-(void)hideKeyboard{
    [_latTextField resignFirstResponder];
    [_lonTextField resignFirstResponder];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"ShowDetails"]){
        StationDetailsViewController *vc = (StationDetailsViewController *)[segue destinationViewController];
        NSDictionary *station;
        for(NSDictionary *stat in _stationList){
            if([[stat objectForKey:@"StationId"] isEqualToString:currentStation])
                station = [[NSDictionary alloc] initWithDictionary:stat];
        }
        vc.stationId = [[NSString alloc] initWithString:[station objectForKey:@"StationId"]];
        vc.stationName = [[NSString alloc] initWithString:[station objectForKey:@"StationName"]];
        vc.providerId = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%ld",(long)[[station objectForKey:@"ProviderId"] integerValue]]];
        vc.providerName = [[NSString alloc] initWithString:[station objectForKey:@"ProviderName"]];
        vc.latitude = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%f",[[station objectForKey:@"Latitude"] doubleValue]]];
        vc.longitude = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%f",[[station objectForKey:@"Longitude"] doubleValue]]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
