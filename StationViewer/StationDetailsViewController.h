//
//  StationDetailsViewController.h
//  StationViewer
//
//  Created by Dominic Ong on 7/3/14.
//  Copyright (c) 2014 Dominic Ong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationDetailsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *stationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *stationIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *providerIdLabel;

@property NSString *latitude;
@property NSString *longitude;
@property NSString *stationId;
@property NSString *stationName;
@property NSString *providerId;
@property NSString *providerName;

@end
